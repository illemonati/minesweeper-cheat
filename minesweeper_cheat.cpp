#include<Windows.h>
#include<iostream>
#include<string.h>
#include<vector>

using namespace std;

DWORD get_pid(string window_name) {
    HWND hwnd = FindWindowA(NULL, window_name.c_str());
    DWORD pid;
    GetWindowThreadProcessId(hwnd, &pid);
    return pid;
}

int read_single_cell(long address, HANDLE handle) {
    BYTE cell;
    ReadProcessMemory(handle, reinterpret_cast<LPVOID>(address), &cell, sizeof(cell), NULL);
    return static_cast<int>(cell);
}

bool write_single_cell(long address, HANDLE handle, BYTE state) {
    return WriteProcessMemory(handle, reinterpret_cast<LPVOID>(address), &state, sizeof(BYTE), NULL);
}


vector<vector<int>> get_board(HANDLE handle, int row_size) {

    vector<vector<int>> board(9, std::vector<int>(row_size, 0));
    long board_start = 0x01005361;
    long offset = 0;
    int i = 0;
    while (true) {
        for (int j = 0; j < row_size; ++j) {
            board[i][j] = read_single_cell(board_start+offset, handle);
            ++offset;
        }
        ++offset;
        while(true) {
            // cout << read_single_cell(board_start+offset, handle) << " " << offset << endl;
            if (read_single_cell(board_start+offset, handle) == 0x10) {
                goto out;
            }
            ++offset;
        }
out:
        ++offset;
        ++i;
        if (read_single_cell(board_start+offset, handle) == 0x10) {
            return board;
        }
    }
}


void instant_solve(HANDLE handle, int row_size, string window_name) {
    long board_start = 0x01005361;
    long offset = 0;
    int i = 0;
    // Work around to update the actual game
    HWND hwnd = FindWindowA(NULL, window_name.c_str());
    bool was_min = IsIconic(hwnd);
    ShowWindow(hwnd, SW_MINIMIZE);
    while (true) {
        for (int j = 0; j < row_size; ++j) {
            int cell = read_single_cell(board_start+offset, handle);
            // write_single_cell(board_start+offset, handle, 0x0f);

            if (cell == 0x8f) {
                write_single_cell(board_start+offset, handle, 0x8e);
            } else {
                // write_single_cell(board_start+offset, handle, 0x40);
            }
            ++offset;
        }
        ++offset;
        while(true) {
            // cout << read_single_cell(board_start+offset, handle) << " " << offset << endl;
            if (read_single_cell(board_start+offset, handle) == 0x10) {
                goto out;
            }
            ++offset;
        }
out:
        ++offset;
        ++i;
        if (read_single_cell(board_start+offset, handle) == 0x10) {
            if (was_min) {
                ShowWindow(hwnd, SW_MINIMIZE);
            } else {
                ShowWindow(hwnd, SW_NORMAL);
            }
            return;
        }
    }
}


const string KEY =
    "E = Empty (Not Clicked)\n"
    "M = Mine (There is a Mine but not clicked)\n"
    "1 = There is one mine around\n"
    "2 = There are two mines around\n"
    "3 = There are three mines around\n"
    "C = Empty (Clicked but there is nothing there)\n"
    "W = Wrong Flag (Flagged but there is no mine there)\n"
    "R = Right Flag (Flagged and there is a mine there)";


char cell_to_char(int cell) {
    switch (cell) {
        case 0x0f:
            return 'E';
        case 0x8f:
            return 'M';
        case 0x41:
            return '1';
        case 0x42:
            return '2';
        case 0x43:
            return '3';
        case 0x40:
            return 'C';
        case 0x0e:
            return 'W';
        case 0x8e:
            return 'R'; 
    }
    return '0';
}

void print_board(vector<vector<int>> board) {
    for (auto row : board) {
        for (auto col : row) {
            cout << cell_to_char(col) << " ";
        }
        cout << endl;
    }
}




int get_row_size(HANDLE handle) {
    long board_start = 0x01005361;
    int count = 0;
    while (true) {
        if (read_single_cell(board_start+count, handle) == 0x10) {
            return count;
        }
        count++;
    }
}


int main(int argc, char ** argv) {
    DWORD pid = get_pid("Minesweeper");
    cout << "Minesweeper Pid : " << pid << endl;

    HANDLE handle = OpenProcess(PROCESS_ALL_ACCESS, false, pid);

    int row_size = get_row_size(handle);
    cout << "Row Size : " << row_size << endl;
    auto board = get_board(handle, row_size);
    cout << KEY << endl;
    print_board(board);

solve:
    cout << "Solve?" << endl;
    char answer;
    cin >> answer;
    cin.ignore();

    switch (answer) {
        case 'y':
            instant_solve(handle, row_size, "MineSweeper");
            break;
        case 'n':
            break;
        default:
            cout << "Could not understand input! Try again!" << endl;
            goto solve;
    }

    return 0;
}

